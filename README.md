Sevaun is a widely-respected, seasoned executive and formidable communicator with a track-record of building relationships across a diverse set players to achieve results. As Chief Communications Officer for one of Canadas leading companies, Sevaun leads communications and corporate social responsibility across Rogers multiple platforms including wireless, cable, business services, TV, radio and sports.

Website: [http://www.rogers.com](http://www.rogers.com)
